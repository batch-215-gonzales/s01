<<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sample Page</title>
</head>
<body>
	<h1>Sample Page by the PageController</h1>
	{{ $frontend }}

	@if(count($topics) > 0)
		@foreach($topics as $topic)
			<li>{{ $topic }}</li>
		@endforeach
	@endif
</body>
</html>
